﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodingTest.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class CountIt : ICountIt
    {
        /// <summary>
        /// Count of all words in document
        /// </summary>
        public int WordCount { get; private set; }

        /// <summary>
        /// Dictionary: keys are words in document, value are occurences of that word
        /// </summary>
        public Dictionary<string, int> WordOccurenceDict { get; private set; }

        /// <summary>
        /// Sorted keys from WordOccurenceDict
        /// </summary>
        public List<string> SortedKeys { get; private set; }

        /// <summary>
        /// Scrambled strings from SortedKeys List
        /// </summary>
        public List<string> ScrambledWordsList { get; private set; }

        private string _document;

        /// <summary>
        /// Setter replaces digits with a space and converts string to lower
        /// </summary>
        private string Document
        {   get
            {
                return this._document;
            }
            set
            {
                _document = Regex.Replace(value, @"\d", " ").ToLowerInvariant();
            }
        }

        IMergeSorter _mergeSorter;
        IScrambler _scrambler;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mergeSorter"></param>
        /// <param name="scrambler"></param>
        public CountIt(IMergeSorter mergeSorter, IScrambler scrambler)
        {
            _mergeSorter = mergeSorter;
            _scrambler = scrambler;


            this.WordCount = 0;
            this.WordOccurenceDict = new Dictionary<string, int>();
        }

        /// <summary>
        /// Processes document: Will populate 'WordOccurenceDict', Calculate total 'WordCount', sort all key and assign the to
        /// SortedKeys and scramble all string of SortedKeys and assign them to ScrambledWordsList
        /// </summary>
        /// <param name="document"></param>
        public void ProcessDocument(string document)
        {
            this.Document = document ?? throw new ArgumentNullException();
            PopulateDict();
            CalcWordCount();
            var allKeys = new List<string>(this.WordOccurenceDict.Keys);
            this.SortedKeys = (allKeys.Count <= 1) ? allKeys : _mergeSorter.Sort(allKeys);
            this.ScrambledWordsList = (allKeys.Count == 0) ? this.SortedKeys : _scrambler.ScrambleWords(this.SortedKeys);
        }

        /// <summary>
        /// populates WordOccurenceDict: keys are words in document, value are occurences of that word
        /// </summary>
        private void PopulateDict()
        {
            string[] separators = new string[] { ",", ".", "!", "?", " " };

            foreach (string word in this.Document.Split(separators, StringSplitOptions.RemoveEmptyEntries))
            {
                if (this.WordOccurenceDict.ContainsKey(word))
                {
                    this.WordOccurenceDict[word]++;
                }
                else
                {
                    this.WordOccurenceDict.Add(word, 1);
                }
            }
        }

        private void CalcWordCount()
        {
            this.WordCount = this.WordOccurenceDict.Sum(x => x.Value);
        }

    }
}
