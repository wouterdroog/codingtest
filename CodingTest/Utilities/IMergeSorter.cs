﻿using System.Collections.Generic;

namespace CodingTest.Utilities
{
    public interface IMergeSorter
    {
        List<string> Sort(List<string> inputList);
    }
}