﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodingTest.Utilities
{
    /// <summary>
    /// Implementation of the merge sort algorithm
    /// </summary>
    public class MergeSorter : IMergeSorter
    {
        /// <summary>
        /// Returns a sorted list
        /// </summary>
        /// <param name="inputList"></param>
        /// <returns></returns>
        public List<string> Sort(List<string> inputList)
        {
            if (inputList == null)
            {
                throw new ArgumentNullException();
            }

            if (inputList.Count == 0 || inputList.Count == 1)
            {
                return inputList;
            }

            // split list
            int inputListLen = inputList.Count;
            int middle = inputListLen / 2;
            var leftList = inputList.Take(middle).ToList();
            var rightList = inputList.Skip(middle).ToList();

            return Merge(Sort(leftList), Sort(rightList));
        }

        private List<string> Merge(List<string> leftList, List<string> rightList)
        {
            var result = new List<string>();
            int leftIndex = 0;
            int rightIndex = 0;

            while (leftIndex < leftList.Count && rightIndex < rightList.Count)
            {
                if (leftList[leftIndex].CompareTo(rightList[rightIndex]) < 1)
                {
                    result.Add(leftList[leftIndex]);
                    leftIndex++;
                }
                else
                {
                    result.Add(rightList[rightIndex]);
                    rightIndex++;
                }
            }

            return result.Concat(leftList.Skip(leftIndex).Concat(rightList.Skip(rightIndex))).ToList();
        }
    }
}
