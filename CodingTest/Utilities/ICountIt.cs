﻿using System.Collections.Generic;

namespace CodingTest.Utilities
{
    public interface ICountIt
    {
        List<string> SortedKeys { get; }
        List<string> ScrambledWordsList { get; }
        int WordCount { get; }
        Dictionary<string, int> WordOccurenceDict { get; }

        void ProcessDocument(string document);
    }
}