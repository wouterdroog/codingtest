﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodingTest.Utilities
{
    /// <summary>
    /// Scrambler scrambles words and circumvents CIA and Russion monitoring
    /// </summary>
    public class Scrambler : IScrambler
    {
        private readonly StringBuilder _stringBuilder = new StringBuilder();

        /// <summary>
        /// Scrambles all string in the list
        /// string: Reverse letters and capitalize every even letter
        /// </summary>
        /// <param name="inputWords"></param>
        /// <returns></returns>
        public List<string> ScrambleWords(List<string> inputWords)
        {       
            if (inputWords == null)
            {
                throw new ArgumentNullException();
            }

            if (inputWords.Count == 0)
            {
                return inputWords;
            }

            var scrambledWords = new List<string>();

            foreach (var word in inputWords)
            {
                switch (word.Length)
                {
                    case 1:
                        scrambledWords.Add(word);
                        break;
                    default:
                        scrambledWords.Add(ScrambleWord(word));
                        break;
                }
            }

            return scrambledWords;
        }

        /// <summary>
        /// Reverse letters and capitalize every even letter
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private string ScrambleWord(string word)
        {
            _stringBuilder.Clear();

            for (int i = word.Length; i > 0; i--)
            {
                char currentChar = word[i - 1];
                if ((word.Length - i) % 2 == 0)
                {
                    _stringBuilder.Append(currentChar);
                }
                else
                {
                    char temp = char.ToUpperInvariant(word[i - 1]);
                    _stringBuilder.Append(char.ToUpperInvariant(currentChar));
                }         
            }

            return _stringBuilder.ToString();
        }
    }
}
