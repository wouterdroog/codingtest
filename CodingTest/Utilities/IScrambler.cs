﻿using System.Collections.Generic;

namespace CodingTest.Utilities
{
    public interface IScrambler
    {
        List<string> ScrambleWords(List<string> inputWords);
    }
}