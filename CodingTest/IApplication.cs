﻿namespace CodingTest
{
    public interface IApplication
    {
        void Run();
    }
}