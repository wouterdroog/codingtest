﻿using CodingTest.Utilities;
using System;

namespace CodingTest
{
    public class Application : IApplication
    {
        ICountIt _countIt;

        public Application(ICountIt countIt)
        {
            _countIt = countIt;
        }


        public void Run()
        {
            string document = "The big brown fox number 4 jumped over the lazy dog. THE BIG BROWN FOX JUMPED OVER THE LAZY DOG. The Big Brown Fox 123.";
            _countIt.ProcessDocument(document);

            CreateConsoleOutput();
        }

        private void CreateConsoleOutput()
        {
            Console.WriteLine($"Number of words: {_countIt.WordCount}");
            Console.WriteLine();

            foreach (var key in _countIt.SortedKeys)
            {
                Console.WriteLine($"{key} {_countIt.WordOccurenceDict[key]}");
            }

            Console.WriteLine();

            foreach (var word in _countIt.ScrambledWordsList)
            {
                Console.WriteLine($"{word}");
            }
        }
    }
}
