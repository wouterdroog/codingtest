﻿using Autofac;
using System.Linq;
using System.Reflection;

namespace CodingTest
{
    /// <summary>
    /// Dependency injection container
    /// </summary>
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Application>().As<IApplication>();

            // Map all Utillity classes on there interfaces
            builder.RegisterAssemblyTypes(Assembly.Load(nameof(CodingTest)))
                .Where(t => t.Namespace.Contains("Utilities"))
                .As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "I" + t.Name));

            return builder.Build();
        }

    }
}
