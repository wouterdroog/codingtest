using CodingTest.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace UnitTest
{
    /// <summary>
    /// Tests for CountIt class
    /// </summary>
    [TestClass]
    public class CountItTest
    {
        Mock<IMergeSorter> mockMergeSorter;
        Mock<IScrambler> mockScrambler;
        CountIt countItSimple;

        [TestInitialize]
        public void InitializeMocks()
        {
            mockMergeSorter = new Mock<IMergeSorter>(MockBehavior.Strict);
            mockScrambler = new Mock<IScrambler>(MockBehavior.Strict);
        }

        public void MockSetUpSimple()
        {
            mockMergeSorter.Setup(s => s.Sort(It.IsAny<List<string>>())).Returns(It.IsAny<List<string>>());
            mockScrambler.Setup(s => s.ScrambleWords(It.IsAny<List<string>>())).Returns(It.IsAny<List<string>>());
            countItSimple = new CountIt(mockMergeSorter.Object, mockScrambler.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CountIt_DocumentIsNull_ThrowArgumentNullException()
        {
            MockSetUpSimple();

            countItSimple.ProcessDocument(null);
        }


        [TestMethod]
        public void CountIt_DocumentWithNumbers_ShouldReplaceNumbersWithSpace1()
        {
            string document = "2";
            int expectedWordCount = 0;
            var wordList = new List<string>() { };

            MockSetUpSimple();
            countItSimple.ProcessDocument(document);

            mockMergeSorter.Verify(v => v.Sort(It.IsAny<List<string>>()), Times.Never);
            mockScrambler.Verify(v => v.ScrambleWords(It.IsAny<List<string>>()), Times.Never);

            Assert.AreEqual(expectedWordCount, countItSimple.WordCount);
        }


        [TestMethod]
        public void CountIt_DocumentWithNumbers_ShouldReplaceNumbersWithSpace2()
        {
            string document = "b2a";
            int expectedWordCount = 2;
            var unsortedList = new List<string>() { "b", "a" };
            var sortedList = new List<string>() { "a", "b" };

            mockMergeSorter.Setup(s => s.Sort(It.IsAny<List<string>>())).Returns(() => { return sortedList; });
            mockScrambler.Setup(s => s.ScrambleWords(It.IsAny<List<string>>())).Returns(It.IsAny<List<string>>());

            var countIt = new CountIt(mockMergeSorter.Object, mockScrambler.Object);

            countIt.ProcessDocument(document);

            mockMergeSorter.Verify(v => v.Sort(unsortedList), Times.Once);
            mockScrambler.Verify(v => v.ScrambleWords(sortedList), Times.Once);

            Assert.AreEqual(expectedWordCount, countIt.WordCount);
        }


        [TestMethod]
        public void CountIt_DocumentWithCasing_ShouldConvertToLower()
        {
            string document = "C";
            var expected = new List<string>() { "c" };

            MockSetUpSimple();

            countItSimple.ProcessDocument(document);

            mockMergeSorter.Verify(v => v.Sort(It.IsAny<List<string>>()), Times.Never);
            mockScrambler.Verify(v => v.ScrambleWords(It.IsAny<List<string>>()), Times.Once);

            CollectionAssert.AreEqual(expected, countItSimple.SortedKeys);
        }



        [TestMethod]
        public void CountIt_DocumentWithPunctuation_ShouldSeperateWordsFromPunctuation()
        {
            string document = "?a!a,a.a";
            int expectedWordCount = 4;
            var expectedSortedKeys = new List<string>() { "a" };

            MockSetUpSimple();

            countItSimple.ProcessDocument(document);

            mockMergeSorter.Verify(v => v.Sort(It.IsAny<List<string>>()), Times.Never);
            mockScrambler.Verify(v => v.ScrambleWords(It.IsAny<List<string>>()), Times.Once);

            CollectionAssert.AreEqual(expectedSortedKeys, countItSimple.SortedKeys);
            Assert.AreEqual(expectedWordCount, countItSimple.WordCount);
        }


        [TestMethod]
        public void CountIt__CanCountTotalWords()
        {
            string document = " ?a ab a b ! A11b a c 77";
            int expectedWordCount = 8;

            MockSetUpSimple();

            countItSimple.ProcessDocument(document);


            mockMergeSorter.Verify(v => v.Sort(It.IsAny<List<string>>()), Times.AtLeastOnce);
            mockScrambler.Verify(v => v.ScrambleWords(It.IsAny<List<string>>()), Times.Once);

            Assert.AreEqual(expectedWordCount, countItSimple.WordCount);
        }


        [TestMethod]
        public void CountIt_CanCountWordOccurences()
        {
            string document = " AA2aa Aa b a aa Bb b!";
            string dictKey_aa = "aa";
            string dictKey_b = "b";
            int expectedOccurences_aa = 4;
            int expectedOccurences_b = 2;

            MockSetUpSimple();

            countItSimple.ProcessDocument(document);

            mockMergeSorter.Verify(v => v.Sort(It.IsAny<List<string>>()), Times.AtLeastOnce);
            mockScrambler.Verify(v => v.ScrambleWords(It.IsAny<List<string>>()), Times.Once);

            Assert.AreEqual(expectedOccurences_aa, countItSimple.WordOccurenceDict[dictKey_aa]);
            Assert.AreEqual(expectedOccurences_b, countItSimple.WordOccurenceDict[dictKey_b]);
        }


        [TestMethod]
        public void CountIt_SortedKeysProperty_IsSorted()
        {
            string document = " AA2aa A� b a aa Bb b!";
            List<string> unsortedList = new List<string>() { "aa", "a�", "b", "a", "bb" };
            List<string> sortedList = new List<string> { "a", "aa", "a�", "b", "bb" };

            mockMergeSorter.Setup(s => s.Sort(It.IsAny<List<string>>())).Returns(() => { return sortedList; } );
            mockScrambler.Setup(s => s.ScrambleWords(It.IsAny<List<string>>())).Returns(It.IsAny<List<string>>());

            var CountIt = new CountIt(mockMergeSorter.Object, mockScrambler.Object);

            CountIt.ProcessDocument(document);

            mockMergeSorter.Verify(v => v.Sort(unsortedList), Times.AtLeastOnce);
            mockScrambler.Verify(v => v.ScrambleWords(sortedList), Times.Once);

            CollectionAssert.AreEqual(sortedList, CountIt.SortedKeys);
        }
    }


    /// <summary>
    /// Test for MergeSorter class
    /// </summary>
    [TestClass]
    public class MergeSortTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MergeSorter_PassingNullArgument_ThrowArgumentNullException()
        {
            var MergeSorter = new MergeSorter();

            MergeSorter.Sort(null);
        }


        [TestMethod]
        public void MergeSorter_PassingEmptyList_ShouldreturnEmptyList()
        {
            var MergeSorter = new MergeSorter();

            List<string> emptyList = new List<string>();
            List<string> resultList = MergeSorter.Sort(emptyList);

            Assert.AreEqual(emptyList, resultList);
        }

        [TestMethod]
        public void MergeSorter_PassingOneWordList_ShouldreturnOneWordList()
        {
            var MergeSorter = new MergeSorter();

             List<string> oneWordList = new  List<string> { "zz" };
             List<string> resultList = MergeSorter.Sort(oneWordList);

            Assert.AreEqual(oneWordList, resultList);
        }


        [TestMethod]
        public void MergeSorter_PassingSortedList_ShouldreturnSortedList()
        {
            var MergeSorter = new MergeSorter();

             List<string> sortedList = new  List<string> { "aa", "ab" };
             List<string> resultList = MergeSorter.Sort(sortedList);

            CollectionAssert.AreEqual(sortedList, resultList);
        }


        [TestMethod]
        public void MergeSorter_PassingUnSortedList_ShouldreturnSortedList()
        {
            var MergeSorter = new MergeSorter();

             List<string> unsortedList = new  List<string> { "ab", "aa", "aap", "aba", "ba", "a", "�", "a" };
             List<string> resultList = MergeSorter.Sort(unsortedList);

            Assert.IsTrue(IsSorted<string>(resultList.ToArray()));
        }


        public static bool IsSorted<T>(T[] array) where T : IComparable<T>
        {
            if (array.Length <= 1)
                return true;

            for (int i = 1; i < array.Length; i++)
            {
                var temp = array[i - 1].CompareTo(array[i]);
                if (array[i - 1].CompareTo(array[i]) > 0)
                    return false;
            }

            return true;
        }
    }



    /// <summary>
    /// Test for Scrambler class
    /// </summary>
    [TestClass]
    public class ScramblerTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Scrambler_PassingNullArgument_ThrowArgumentNullException()
        {
            var Scrambler = new Scrambler();

            Scrambler.ScrambleWords(null);
        }


        [TestMethod]
        public void Scrambler_PassingEmptyList_ShouldreturnEmptyList()
        {
            var Scrambler = new Scrambler();

            var emptyList = new List<string>();
            var resultList = Scrambler.ScrambleWords(emptyList);

            CollectionAssert.AreEqual(emptyList, resultList);
        }



        [TestMethod]
        public void Scrambler_PassingOneLetterWord_ReturnSameWord()
        {
            var Scrambler = new Scrambler();

            var inputList = new List<string>() { "�" };
            var resultList = Scrambler.ScrambleWords(inputList);

            CollectionAssert.AreEqual(inputList, resultList);
        }


        [TestMethod]
        public void Scrambler_PassingOneLetterWords_ReturnSameWords()
        {
            var Scrambler = new Scrambler();

            var inputList = new List<string>() { "�", "z" };
            var resultList = Scrambler.ScrambleWords(inputList);

            CollectionAssert.AreEqual(inputList, resultList);
        }


        [TestMethod]
        public void Scrambler_PassingWords_ReturnScrambledWords()
        {
            var Scrambler = new Scrambler();

            var inputList = new List<string>() { "a", "�z", "aap", "lang", "lange", "l�nger" };
            var expected = new List<string>() { "a", "z�", "pAa", "gNaL", "eGnAl", "rEgN�L" };
            var resultList = Scrambler.ScrambleWords(inputList);

            CollectionAssert.AreEqual(expected, resultList);
        }

    }
}
